﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGRepeater : MonoBehaviour
{
	private SpriteRenderer sprite;

	private float spriteWidth;

	void Awake()
	{
		//get the width of the sprite
		sprite = gameObject.GetComponent<SpriteRenderer> ();
		spriteWidth = sprite.bounds.extents.x*2f;
	}

	void Update () 
	{
		//if the sprite has moved a distance equal to sprite width let's call the reposition function
		if (transform.position.x < -spriteWidth)
		{
			RepositionBackground ();
		}


	}

	//this is the function that reposition the sprite so that it can be scrolled again
	private void RepositionBackground()
	{
		Vector2 groundOffset = new Vector2 (spriteWidth * 2f, 0);
		transform.position = (Vector2)transform.position + groundOffset;
	}


}
