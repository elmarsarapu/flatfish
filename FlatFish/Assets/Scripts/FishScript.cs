﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FishScript : MonoBehaviour {

	public float UpForce;

	private Rigidbody2D rb2d;

	private Animator subAnim;

	private bool destroyed;

	public GameObject partSyst;

    public AudioSource upAS;

    void Start()
	{
		//get the rigidbody2d and animator components
		rb2d = gameObject.GetComponent<Rigidbody2D> ();
		subAnim = gameObject.GetComponent<Animator> ();
	}

	void Update()
	{
		if (destroyed == false) 
		{
			//comment standalone controls and uncomment touch controls when you are building to mobile
			//standalone click control

			if (Input.GetMouseButtonDown (0) & !EventSystem.current.IsPointerOverGameObject()) 
			{
				GoUp ();
			}



			//mobile touch control
			/*
			if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) 
			{
				if (!EventSystem.current.IsPointerOverGameObject (Input.GetTouch (0).fingerId)) 
				{
					GoUp ();
				}
			}
			*/

			//according to its vilocity we rotate a little bet our submrine on the Z axis
			if (rb2d.velocity.y >= 0) 
			{
				gameObject.transform.eulerAngles = new Vector3 (0, 0, 2.5f);
			}

			if (rb2d.velocity.y < 0) 
			{
				gameObject.transform.eulerAngles = new Vector3 (0, 0, -2f);
			}
		}

	}

	//this the function called when the fish collides with "spikes" tagged colliders
	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("spikes")) 
		{
			destroyed = true;
			//subAnim.SetTrigger ("distroy");
			partSyst.SetActive (false);
			GMScript.instance.gameOver = true;
		}
	}

	//function called when the fish collides with score trigger colliders
	private void OnTriggerEnter2D (Collider2D col)
	{
		if (col.gameObject.CompareTag ("score")) 
		{
			GMScript.instance.Scored ();
		}
	}


	//function that makes the fish go up
	void GoUp()
	{
		rb2d.AddForce (new Vector2 (0, UpForce));
		rb2d.velocity = Vector2.zero;

        upAS.Stop();
        upAS.Play();
    }
		
}
