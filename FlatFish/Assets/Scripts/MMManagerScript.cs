﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MMManagerScript : MonoBehaviour {

	public GameObject onSoundButton, offSoundButton;

	public AudioSource AmbAS;

	private float volume;

	//play button function (load game scene)
	public void Play()
	{
		SceneManager.LoadScene ("GameScene");
	}
		
	public void Start()
	{
		//get volume saved in playerprefs and apply it to ambiant audio source
		volume = PlayerPrefs.GetFloat ("volume", 1);
		AmbAS.volume = volume;
	}

	//mute button function
	public void Mute()
	{
		onSoundButton.SetActive (true);
		offSoundButton.SetActive (false);
		PlayerPrefs.SetFloat ("volume", 0);
		AmbAS.volume = 0;
	}

	//unmute button function
	public void UnMute()
	{
		onSoundButton.SetActive (false);
		offSoundButton.SetActive (true);
		PlayerPrefs.SetFloat ("volume", 1);
		AmbAS.volume = 1;

	}

	//quit button function
	public void Quit()
	{
		Application.Quit ();
	}

}
