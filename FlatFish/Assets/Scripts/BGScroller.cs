﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BGScroller : MonoBehaviour {

	private Rigidbody2D rb2d;

	private Scene currentScene;


	void Start()
	{
		//get rigidbody2d component
		rb2d = gameObject.GetComponent<Rigidbody2D> ();
		//get the name of the current scene
		currentScene = SceneManager.GetActiveScene ();
	}

	//function that makes the gameobject move on the X axis (scrolling of the backgtround and obstacles), according the scene and tags
	void FixedUpdate()
	{
		if (currentScene.name == "GameScene") 
		{
			
			if (this.CompareTag ("sea")) 
			{
				rb2d.velocity = new Vector2 (GMScript.instance.seaScrollingSpeed, 0);
			} 
			else if (this.CompareTag ("spikes")) 
			{
				rb2d.velocity = new Vector2 (GMScript.instance.spikesScrollingSpeed, 0);
			}

			if (GMScript.instance.gameOver == true) 
			{
				rb2d.velocity = Vector2.zero;
			}
		}

		if (currentScene.name == "MainMenu") 
		{
			if (this.CompareTag ("sea")) 
			{
				rb2d.velocity = new Vector2 (-0.3f, 0);
			} 
			else if (this.CompareTag ("spikes")) 
			{
				rb2d.velocity = new Vector2 (-1, 0);
			}
		}
	}

}
