﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using PartaGames.Social;


public class GMScript : MonoBehaviour {

	public static GMScript instance;

	public float seaScrollingSpeed,spikesScrollingSpeed,obstaclesDelay,volume;

	public bool gameOver=false, goFunctionCalled=false;

	public GameObject pooler,goPanel,newHS,pauseElements,pauseButton, helpText;

	public Text scoreText,yourScore,bestScore;

	private int score,highScore;

	public Animator camAnim;

	public AudioSource scoreSonar,collisionAS,ambiantAS;

	private static readonly string TITLE = "Flat Fish";
	private static readonly string TEXT = "Can you beat my score in Flat Fish?";


	//check there is only one game manager instance going on
	void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
	}

	void Start()
	{
		//get the saved high score from playerprefs file
		highScore = PlayerPrefs.GetInt ("highScore");
		//call a couroutine that will hide the help text
		StartCoroutine (HideHelpText ());

		//get the saved volume from playerprefs file and apply it to audio sources
		volume = PlayerPrefs.GetFloat ("volume",1);
		scoreSonar.volume = volume;
		collisionAS.volume = volume;
		ambiantAS.volume = volume;
	}

	void Update()
	{
		//if the game is over and we didnt call gameover function yet, let's call it
		if (gameOver == true & goFunctionCalled==false) 
		{
			GameOver ();
		}
			
	}

	public void Scored()
	{
		//play the scoring sound, add one to the score and modify the UI score text
		if (gameOver) 
			return;

		scoreSonar.Play ();
		score++;
		scoreText.text = score.ToString ();
	}

	//hide the help message after 2 seconds from the start of the game
	IEnumerator HideHelpText()
	{
		yield return new WaitForSeconds (2f);

		helpText.SetActive(false);
	}

	private void GameOver()
	{
		//stop ambiant sound,play the collision effect, hide game UI, disable the obstacles pooler, and show the GOpanel
		ambiantAS.Stop ();
		collisionAS.Play ();
		camAnim.SetTrigger ("shakeit");
		pauseButton.SetActive (false);
		pooler.SetActive (false);
		scoreText.enabled = false;
		goPanel.SetActive (true);
		yourScore.text = score.ToString ();
		bestScore.text = highScore.ToString ();

		//if the current score is bigger than the saved highscore, we show the "new high score" text and we save the new high score
		if (score > highScore) 
		{
			newHS.SetActive (true);
			PlayerPrefs.SetInt ("highScore", score);
		}
		goFunctionCalled = true;
	}

	//retry button function (reload the game scene)
	public void Retry()
	{
		SceneManager.LoadScene ("GameScene");
	}

	//Home button Function (load MainMenu scene)
	public void BackHome()
	{
		SceneManager.LoadScene ("MainMenu");
	}

	//pause button function
	public void Pause()
	{
		Time.timeScale = 0;
		pauseElements.SetActive (true);
		pauseButton.SetActive (false);
	}


	//resume game button function
	public void unPause()
	{
		Time.timeScale = 1;
		pauseElements.SetActive (false);
		pauseButton.SetActive (true);
	}

	//this is the function used to take a screenshot (called by ShareTextAndImage())
	private string TakeScreenshot()
	{
		var screenshotRenderTexture = new RenderTexture(Screen.width, Screen.height, 16, RenderTextureFormat.ARGB32);
		screenshotRenderTexture.Create();

		int resWidth = screenshotRenderTexture.width;
		int resHeight = screenshotRenderTexture.height;
		Camera.main.targetTexture = screenshotRenderTexture;
		Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
		Camera.main.Render();
		RenderTexture.active = screenshotRenderTexture;
		screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
		Camera.main.targetTexture = null;
		RenderTexture.active = null;
		screenshotRenderTexture.Release();
		byte[] bytes = screenShot.EncodeToPNG();
		string filename = Application.persistentDataPath + "/screenshot.png";
		System.IO.File.WriteAllBytes(filename, bytes);
		return filename;
	}
		

	//share button Function
	public void ShareTextAndImage() 
	{
		NativeShare.Share(TITLE, TEXT, TakeScreenshot());
	}

}
