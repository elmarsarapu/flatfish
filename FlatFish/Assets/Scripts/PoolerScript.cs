﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolerScript : MonoBehaviour {

	public GameObject [] obstacles;
	private float timer;
	private float yPos;

	void Start()
	{
		//get the delay between obstacles from game manager script
		timer = GMScript.instance.obstaclesDelay;
	}

	void FixedUpdate()
	{
			//reduce timer by time
			timer -= Time.deltaTime;
			
		//when timer is less or equal to zero (we have waited for the delay) let's instantiate a new obstacle
			if (timer <= 0) 
			{
				int obsNb = Random.Range (0, 1);

				float yPos = Random.Range (-2.5f, 2.5f);

				Instantiate (obstacles [obsNb], new Vector3 (10f, yPos, 0), Quaternion.identity);

				timer = GMScript.instance.obstaclesDelay;
			}

	}
}
