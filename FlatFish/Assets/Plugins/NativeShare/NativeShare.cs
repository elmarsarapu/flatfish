﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace PartaGames {

    namespace Social { 

        public class NativeShare {

            #if UNITY_IOS
                [DllImport("__Internal")]
                private static extern void _shareStatusAndTitleAndImage(string text, string title, string path);
            #endif

            public static void Share(string title, string text)
            {
                DoShare(null, title, text, null);
            }

            public static void Share(string title, string text, string imagePath)
            {
                DoShare(null, title, text, imagePath);
            }

            public static void Share(string subject, string title, string text, string imagePath)
            {
                DoShare(subject, title, text, imagePath);
            }

            private static void DoShare(string subject, string title, string text, string imagePath)
            {
                if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer) {
                    Debug.LogError("NativeShare: Trying to run on an unsupported platform! Currently supported platforms are: Android & iOS");
                    return;
                }

                #if UNITY_IOS
                _shareStatusAndTitleAndImage(text, title, imagePath);
                #endif

                #if UNITY_ANDROID
                AndroidJavaClass shareClass = new AndroidJavaClass("com.partagames.unity.plugins.nativeshare.NativeShareUtil");
                shareClass.CallStatic("share", subject, title, text, imagePath);
                #endif
            }
        }
    }
}