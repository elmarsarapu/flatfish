//
//  nativeshare_ios.h
//  nativeshare_ios
//
//  Created by antti on 24/08/2017.
//  Copyright © 2017 Parta Games Oy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <nativeshare_ios/NativeShareUIViewController.h>

//! Project version number for nativeshare_ios.
FOUNDATION_EXPORT double nativeshare_iosVersionNumber;

//! Project version string for nativeshare_ios.
FOUNDATION_EXPORT const unsigned char nativeshare_iosVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <nativeshare_ios/PublicHeader.h>


